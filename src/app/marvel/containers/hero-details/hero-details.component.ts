import { Component, OnInit, Input, Output, EventEmitter, ɵConsole } from '@angular/core';
import { HeroInterface } from '../../models/hero-interface';

@Component({
  selector: 'val-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.scss']
})
export class HeroDetailsComponent implements OnInit {

  @Input() hero22 : HeroInterface;
  hero2 : HeroInterface;
  hero3 : HeroInterface;
  fich : string;
  @Output() onHeroClicked : EventEmitter<HeroInterface> = new EventEmitter();

  selectHero(selectedHero : HeroInterface) : void{
    console.log('selectedHero', selectedHero);
    this.onHeroClicked.emit(selectedHero) ;
  }

  constructor() { }

  ngOnInit() {
    this.hero2 = this.hero22;
    this.hero3 = this.hero22;
    this.fich = this.hero22.file;
  }
}