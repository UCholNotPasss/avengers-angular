export interface HeroInterface {
    id : number;
    name : string;
    nickname : string;
    firstscene : Date;
    file : string;
}