import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HeroInterface } from '../../models/hero-interface';

@Component({
  selector: 'val-hero-list-item',
  templateUrl: './hero-list-item.component.html',
  styleUrls: ['./hero-list-item.component.scss']
})
export class HeroListItemComponent implements OnInit {

  hero4 : HeroInterface;
  heroes : HeroInterface[] = [
    {id:1, name:'Steve Rogers', nickname:'Captain America', firstscene:new Date(2011, 8, 17), file:'assets/images/captain.png'},
    {id:2, name:'Bruce Banner', nickname:'Hulk', firstscene:new Date(2003, 7, 2), file:'assets/images/hulk.png'},
    {id:3, name:'Tony Stark', nickname:'Iron Man', firstscene:new Date(2008, 4, 30), file:'assets/images/ironman.png'},
    {id:4, name:'Groot', nickname:'Je s\'appelle Groot', firstscene:new Date(2014, 8, 13), file:'assets/images/groot.png'},
    {id:5, name:'Rocket', nickname:'Rocket', firstscene:new Date(2014, 8, 13), file:'assets/images/rocket.png'},
    {id:6, name:'Loki', nickname:'Loki', firstscene:new Date(2011, 4, 27), file:'assets/images/loki.png'},
    {id:7, name:'Stephen Strange', nickname:'Doctor Strange', firstscene:new Date(2016, 10, 26), file:'assets/images/strange.png'},
    {id:8, name:'Thor', nickname:'Thor', firstscene:new Date(2011, 4, 27), file:'assets/images/thor.png'},
    {id:9, name:'Peter Quill', nickname:'Starlord', firstscene:new Date(2014, 8, 13), file:'assets/images/starlord.png'},
    {id:10, name:'Clint Barton', nickname:'Hawkeye', firstscene:new Date(2011, 4, 27), file:'assets/images/hawkeye.png'}
  ];
  
  constructor() { }

  ngOnInit() {
    //this.hero = this.heroes[this.id];
    this.hero4 = this.heroes[1];
  }

}
