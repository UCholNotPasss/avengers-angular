import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HerosComponent } from './containers/heros/heros.component';
import { HeroDetailsComponent } from './containers/hero-details/hero-details.component';

const routes: Routes = [
	{path:'',component:HerosComponent},
	{path:':id/details',component:HeroDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarvelRoutingModule { }