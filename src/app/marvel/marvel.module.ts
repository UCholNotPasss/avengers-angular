import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarvelRoutingModule } from './marvel-routing.module';
import { HerosComponent } from './containers/heros/heros.component';
import { HeroDetailsComponent } from './containers/hero-details/hero-details.component';
import { HeroListItemComponent } from './components/hero-list-item/hero-list-item.component';

@NgModule({
  declarations: [
    HerosComponent, 
    HeroDetailsComponent, 
    HeroListItemComponent
  ],
  imports: [
    CommonModule,
    MarvelRoutingModule
  ]
})
export class MarvelModule { }
