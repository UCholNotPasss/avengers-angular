import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
	//{path:'',redirectTo:'not-found', pathMatch:'full'},
	{path:'', loadChildren:'./marvel/marvel.module#MarvelModule'},
	{path:'heroes', loadChildren:'./marvel/marvel.module#MarvelModule'},
	{path:'not-found',component:NotFoundComponent},
	{path:'**', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true, enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }